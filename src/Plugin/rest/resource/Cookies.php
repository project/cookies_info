<?php

namespace Drupal\cookies_info\Plugin\rest\resource;

use Drupal\cookies_info\Cookies\Collector;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Cookies resource.
 *
 * @RestResource(
 *   id = "cookies_info_resource",
 *   label = @Translation("Cookies Info Resource"),
 *   uri_paths = {
 *     "canonical" = "/rest/cookies_info/collection"
 *   }
 * )
 */
class Cookies extends ResourceBase {

  /**
   * Collector service.
   *
   * @var \Drupal\cookies_info\Cookies\Collector
   */
  protected $collector;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('cookies_info.collector')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, Collector $collector) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->collector = $collector;
  }

  /**
   * Responds to entity GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Cookies collection.
   */
  public function get() {
    $response = ['cookies' => $this->collector->getCollection()];
    return new ResourceResponse($response);
  }

}

<?php

namespace Drupal\cookies_info\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class Settings.
 *
 * @package Drupal\cookies_info\Form
 */
class Settings extends ConfigFormBase {

  /**
   * User storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->userStorage = $entity_type_manager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['cookies_info.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cookie_info_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $user_field = $this->configFactory()->getEditable('cookies_info.settings')->get('settings.privileged_user');
    if (!empty($user_field)) {
      /** @var \Drupal\Core\Session\AccountProxyInterface[] $users */
      $users = $this->userStorage->loadByProperties(['name' => $user_field]);
      $user_field = reset($users);
      if (!($user_field->hasPermission('cookie info allow to communicate with a cookie server') && $user_field->hasPermission('restful get cookies_info_resource'))) {
        $this->messenger()->addWarning($this->t(
          'User haven\'t required permissions. See <a href="@url" target="_blank">status report page</a> for details.',
          ['@url' => Url::fromRoute('system.status')->toString()]
        ));
      }
    }

    $form = parent::buildForm($form, $form_state);
    $form['settings'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['settings']['privileged_user'] = [
      '#title' => $this->t('Select Privileged User'),
      '#type' => 'entity_autocomplete',
      '#default_value' => empty($user_field) ? NULL : $user_field,
      '#target_type' => 'user',
      '#selection_settings' => [
        'include_anonymous' => FALSE,
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $field_path = ['settings', 'privileged_user'];
    $value = $form_state->getValue($field_path);
    if (!empty($value)) {
      $user = $this->loadUserByValue($value);
      if ($user && in_array('administrator', $user->getRoles())) {
        $form_state->setErrorByName(implode('][', $field_path), $this->t("Privileged user shouldn't be related to the Administrator role"));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user_field_name = ['settings', 'privileged_user'];
    $user_field_value = $form_state->getValue($user_field_name, '');
    if (!empty($user_field_value)) {
      $user_field_value = $this->loadUserByValue($user_field_value)
        ->getAccountName();
    }
    $this->configFactory()->getEditable('cookies_info.settings')
      ->set('settings.privileged_user', $user_field_value)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Helper too load user by the form state value.
   *
   * @param mixed $value
   *   Input value.
   *
   * @return \Drupal\user\UserInterface|\Drupal\Core\Entity\EntityInterface|null
   *   User instance.
   */
  protected function loadUserByValue($value) {
    return $this->userStorage->load($value);
  }

}

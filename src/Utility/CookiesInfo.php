<?php

namespace Drupal\cookies_info\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\jwt\Authentication\Provider\JwtAuth;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\PermissionHandlerInterface;

/**
 * Class CookiesInfo.
 *
 * @package Drupal\cookies_info\Utility
 */
final class CookiesInfo {

  use StringTranslationTrait;

  /**
   * Privileged user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * User storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $userStorage;

  /**
   * JWT service.
   *
   * @var \Drupal\jwt\Authentication\Provider\JwtAuth
   */
  protected $jwt;

  /**
   * Permission service.
   *
   * @var \Drupal\user\PermissionHandlerInterface
   */
  protected $permission;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * CookiesInfo constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\jwt\Authentication\Provider\JwtAuth $jwt
   *   JWT service.
   * @param \Drupal\user\PermissionHandlerInterface $user_permissions
   *   Permissions service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, JwtAuth $jwt, PermissionHandlerInterface $user_permissions, ConfigFactoryInterface $config) {
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->jwt = $jwt;
    $this->permission = $user_permissions;
    $this->config = $config;
  }

  /**
   * Hook requirements helper.
   *
   * @return array[]|void
   *   Requirements definitions.
   */
  public function requirements() {
    return [
      self::class . 'User' => $this->userRequirements(),
      self::class . 'UserPermission' => $this->userPermissionRequirements(),
      self::class . 'JwtSettingsConfigured' => $this->jwtSettingsConfigured(),
    ];
  }

  /**
   * User defined in the settings.
   *
   * @return \Drupal\Core\Session\AccountProxyInterface|null
   *   User instance, null - otherwise.
   */
  protected function loadUser() {
    $user_uuid = $this->config->get('cookies_info.settings')->get('settings.privileged_user');
    $user = NULL;
    if (!empty($user_uuid)) {
      $result = $this->userStorage->loadByProperties(['name' => $user_uuid]);
      $result = reset($result);
      if ($result) {
        $user = $result;
      }
    }
    return $user;
  }

  /**
   * Title template for requirements.
   *
   * @param string $title
   *   Unique title.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Template result.
   */
  protected function titleTemplate($title) {
    return $this->t('Cookies Info: <br/> @title', ['@title' => $title]);
  }

  /**
   * User requirement builder.
   */
  protected function userRequirements() {
    $requirement['title'] = $this->titleTemplate($this->t('Granted user'));
    $requirement['description'] = $this->t(
      'Module requires to define granted user at the <a href=":url" target="_blank">settings page</a>.',
      [':url' => Url::fromRoute('cookies_info.admin_settings')->toString()]
    );

    $ok = FALSE;
    $user = $this->loadUser();
    if ($user) {
      $this->user = $user;
      $ok = TRUE;
    }

    $requirement['value'] = $ok ?
      $this->t(
        '<a href=":link" target="_blank">%user</a> user defined as privileged user',
        [
          '%user' => $this->user->getAccountName(),
          ':link' => Url::fromRoute(
            'entity.user.canonical',
            ['user' => $this->user->id()]
          )->toString(),
        ]
      ) :
      $this->t('Not defined');
    $requirement['severity'] = $ok ? REQUIREMENT_OK : REQUIREMENT_ERROR;
    return $requirement;
  }

  /**
   * User permission requirement.
   *
   * @return array
   *   Permission requirement.
   */
  protected function userPermissionRequirements() {
    $requirement['title'] = $this->titleTemplate($this->t('Granted user permissions'));

    $messages = [];
    $user_set = $is_admin_role = FALSE;
    if (!empty($this->user)) {
      $user_set = TRUE;

      $is_admin_role = in_array('administrator', $this->user->getRoles());
      if ($is_admin_role) {
        $messages[] = $this->t("Security issue: privileged user shouldn't be related to the Administrator role");
      }

      $permissions = $this->permission->getPermissions();
      $required_permissions = [
        'cookie info allow to communicate with a cookie server',
        'restful get cookies_info_resource',
      ];
      foreach ($required_permissions as $permission) {
        if (!$this->user->hasPermission($permission)) {
          $messages[] = strip_tags($permissions[$permission]['title']);
        }
      }
    }
    $ok = empty($messages) && $user_set && !$is_admin_role;
    $requirement['value'] = $ok ?
      $this->t('All permissions set') :
      $this->t('Permissions NOT set for the privileged user:');
    if ($user_set) {
      if ($ok) {
        $requirement['description'] = $this->t('Granted user has all required permissions.');
      }
      else {
        $requirement['description'] = $this->t(
          '<ol><li>%messages</li></ol> Defined user from the <a href=":url" target="_blank">settings page</a> should be related to a role that have required permissions from the above list.',
          [
            '%messages' => implode('</li><li>', $messages),
            ':url' => Url::fromRoute('cookies_info.admin_settings')->toString(),
          ]
        );
      }
    }
    else {
      $requirement['description'] = $this->t(
        'Please, define granted user at the <a href=":url" target="_blank">settings page</a>.',
        [':url' => Url::fromRoute('cookies_info.admin_settings')->toString()]
      );
    }
    $requirement['severity'] = $ok ? REQUIREMENT_OK : ($user_set ? REQUIREMENT_ERROR : REQUIREMENT_WARNING);
    return $requirement;
  }

  /**
   * JWT configuration check.
   */
  protected function jwtSettingsConfigured() {
    $requirement['title'] = $this->titleTemplate($this->t('JWT configuration'));
    $requirement['description'] = $this->t('Module requires configured JWT module.');
    $jwt_key = $this->config->get('jwt.config')->get('key_id');
    $ok = !empty($jwt_key);

    $requirement['value'] = $ok ?
      $this->t('Configuration is complete.') :
      $this->t(
        'Follow the <a href=":url" target="_blank">settings page</a> of JWT module and set the proper settings.',
        [':url' => Url::fromRoute('jwt.jwt_config_form')->toString()]
      );
    $requirement['severity'] = $ok ? REQUIREMENT_OK : REQUIREMENT_ERROR;
    return $requirement;
  }

}

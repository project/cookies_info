<?php

namespace Drupal\cookies_info\Cookies;

use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class Collector.
 *
 * @package Drupal\cookies_info\Cookies
 */
final class Collector {

  const STATE_NAME = 'get_cookie.collection';

  /**
   * State service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return \Drupal\cookies_info\Cookies\Collector
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state')
    );
  }

  /**
   * Collector constructor.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   State service.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * Parse cookies collection.
   *
   * @param \Symfony\Component\HttpFoundation\ParameterBag $cookiesBag
   *   Cookies collection.
   *
   * @return $this
   */
  public function parseCookies(ParameterBag $cookiesBag) {
    $this->setCollection(array_keys($cookiesBag->all()));
    return $this;
  }

  /**
   * Set collection value.
   *
   * @param string[] $value
   *   List of the cookie names.
   *
   * @return $this
   */
  protected function setCollection(array $value) {
    $this->state->set(self::STATE_NAME, array_unique(array_merge($this->getCollection(), $value)));
    return $this;
  }

  /**
   * Get collection value.
   *
   * @return string[]
   *   Collection of the cookie names.
   */
  public function getCollection() {
    return $this->state->get(self::STATE_NAME, []);
  }

}

<?php

namespace Drupal\cookies_info\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\cookies_info\Cookies\Collector;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\PostResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EventSubscriber.
 *
 * @package Drupal\cookies_info
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * Collector service.
   *
   * @var \Drupal\cookies_info\Cookies\Collector
   */
  protected $collector;

  /**
   * Settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * EventSubscriber constructor.
   *
   * @param \Drupal\cookies_info\Cookies\Collector $collector
   *   Collector service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   Settings.
   */
  public function __construct(Collector $collector, ConfigFactoryInterface $config) {
    $this->collector = $collector;
    $this->config = $config->get('cookies_info.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['onAggregate'];
    return $events;
  }

  /**
   * Aggregate cookies event subscriber.
   *
   * @param \Symfony\Component\HttpKernel\Event\PostResponseEvent $event
   *   Post response event instance.
   */
  public function onAggregate(PostResponseEvent $event) {
    if ($this->config->get('settings.collector_enabled')) {
      $this->collector->parseCookies($event->getRequest()->cookies);
    }
  }

}
